import {StyleSheet, Text, View, Image} from 'react-native';
import React, {useEffect} from 'react';
import {sa} from '../../assets';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    });
  }, [5000]);

  return (
    <View style={styles.container}>
      <Image source={sa} style={styles.Logo} />
      <Text style={styles.MovieApp}> MY APP</Text>
      <Text style={styles.Name}>Gama</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#73777B',
  },
  Logo: {
    width: 200,
    height: 200,
  },
  MovieApp: {
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
  },
  Name: {
    fontSize: 14,
    position: 'absolute',
    bottom: 18,
    fontWeight: 'bold'
  },
 
});
