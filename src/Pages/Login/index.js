import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import auth from '@react-native-firebase/auth';
import TouchID from 'react-native-touch-id';
import {GoogleSignin} from '@react-native-google-signin/google-signin';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const onLogin = () => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        navigation.replace('Home');
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.error(error);
      });
  };

  useEffect(() => {
    GoogleSignin.configure({
      webClientId:
        '515698960568-2qo9ebksvbhmiidmcrnneari96dpjo6l.apps.googleusercontent.com',
    });
  });

  async function onGoogleButtonPress() {
    // Get the users ID token
    const {idToken} = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }

  const Touch = () => {
    const optionalConfigObject = {
      title: 'Authentication Required', // Android
      imageColor: '#e00606', // Android
      imageErrorColor: '#ff0000', // Android
      sensorDescription: 'Touch sensor', // Android
      sensorErrorDescription: 'Failed', // Android
      cancelText: 'Cancel', // Android
      fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
      unifiedErrors: false, // use unified error messages (default false)
      passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
    };

    TouchID.authenticate(
      'to demo this react-native component',
      optionalConfigObject,
    )
      .then(success => {
        navigation.navigate('Home');
      })
      .catch(error => {
        Alert.alert('Authentication Failed');
      });
  };
  return (
    <View style={{backgroundColor: '#73777B', flex: 1}}>
      <View style={styles.APP}>
        <Text style={{fontSize: 50}}>MY APP</Text>
      </View>
      <View style={{alignItems: 'center'}}>
        <TextInput
          value={email}
          onChangeText={text => setEmail(text)}
          style={styles.input}
          placeholder="Email"
          placeholderTextColor={'black'}
        />
        <TextInput
          value={password}
          onChangeText={text => setPassword(text)}
          style={styles.input}
          placeholder="Password"
          placeholderTextColor={'black'}
        />
        <View style={{marginLeft: 130, marginBottom: 15}}>
          <Text style={{fontWeight: 'bold'}}>Forgot password?</Text>
        </View>
        <View style={styles.LoginStyle}>
          <TouchableOpacity onPress={() => onLogin()}>
            <View style={styles.Login}>
              <Text>Login</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.LoginStyle}>
          <TouchableOpacity
            onPress={() =>
              onGoogleButtonPress().then(() => alert('Signed in with Google!'))
            }>
            <View style={styles.Google}>
              <Text>Login with Google</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.Biometric}>
          <TouchableOpacity onPress={() => navigation.replace('Home')}>
            <View style={styles.Login}>
              <Text>Login with by Biometric</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  APP: {
    alignItems: 'center',
    marginTop: 40,
    marginBottom: 60,
  },
  input: {
    width: 250,
    height: 50,
    borderWidth: 2,
    borderRadius: 6,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  Login: {
    backgroundColor: '#0093AB',
    width: 200,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  Google: {
    backgroundColor: '#0093AB',
    width: 200,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    fontWeight: 'bold',
  },
  Biometric: {
    backgroundColor: '#0093AB',
    width: 200,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    fontWeight: 'bold',
    marginTop: 20,
  },
});
